import 'package:belajargetxstatemanager/model/orang.dart';
import 'package:get/get.dart';

class OrangController extends GetxController {
  var orang = Orang(nama: "Dapu", umur: 21).obs;

  void changeUpperCase() {
    orang.update((_) {
      orang.value.nama = orang.value.nama.toString().toUpperCase();
    });
  }

  void changeLowerCase() {
    orang.value.nama = orang.value.nama.toLowerCase();
  }
}