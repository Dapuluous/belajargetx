import 'package:belajargetxstatemanager/controller/counter_controller.dart';
import 'package:belajargetxstatemanager/controller/orang_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // final orangC = Get.put(OrangController());
  // final countC = Get.put(CounterController());

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Navigasi"),
        ),
        body: Center(
          child: GetX<CounterController>(
            init: CounterController(),
            builder: (controller) => Text(
              "Angka ${controller.count}",
              style: const TextStyle(fontSize: 30),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Get.find<CounterController>().FuncIncrement();
          },
          child: const Text(
            "+",
            style: TextStyle(fontSize: 30),
          ),
        ),
      ),
    );
  }
}