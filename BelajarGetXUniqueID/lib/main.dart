import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final countC = Get.put(CountController());

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GetBuilder<CountController>(
                id: "data1",
                init: CountController(),
                builder: (_) {
                  return Text(
                    "Angka ${countC.count}",
                  );
                },
              ),

              GetBuilder<CountController>(
                id: "data2",
                init: CountController(),
                builder: (_) {
                  return Text(
                    "Angka ${countC.count}",
                  );
                },
              ),

              GetBuilder<CountController>(
                id: "data3",
                init: CountController(),
                builder: (_) {
                  return Text(
                    "Angka ${countC.count}",
                  );
                },
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Get.find<CountController>().add();
          },
        ),
      ),
    );
  }
}

class CountController extends GetxController {
  var count = 0.obs;

  void add() {
    count++;
    update(['data1']);
  }
}