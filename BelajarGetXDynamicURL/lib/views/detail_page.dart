import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Product Detail"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Product ID: ${Get.parameters['idProduct']}"),

            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Text("Product Name: ${Get.parameters['name']}"),
            ),

            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Text("Product Ukuran: ${Get.parameters['ukuran']}"),
            ),
          ],
        )
      ),
    );
  }
}