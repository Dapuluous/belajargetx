import 'package:belajargetxdynamicurl/routes/routes_name.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("List Product"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () => Get.toNamed(RouteName.product + "/1?name=Jaket&ukuran=XL"),
                child: const Text("Product 1")
            ),

            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: ElevatedButton(
                  onPressed: () => Get.toNamed(RouteName.product + "/2?name=Sepatu&ukuran=XL"),
                  child: const Text("Product 2")
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: ElevatedButton(
                  onPressed: () => Get.toNamed(RouteName.product + "/3?name=Kaos Kaki&ukuran=XL"),
                  child: const Text("Product 3")
              ),
            ),
          ],
        ),
      ),
    );
  }
  
}