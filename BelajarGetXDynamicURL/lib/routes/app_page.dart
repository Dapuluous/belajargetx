import 'package:belajargetxdynamicurl/routes/routes_name.dart';
import 'package:belajargetxdynamicurl/views/detail_page.dart';
import 'package:belajargetxdynamicurl/views/home_page.dart';
import 'package:belajargetxdynamicurl/views/product_page.dart';
import 'package:get/get.dart';

class AppPage {
  static final pages = [
    GetPage(name: RouteName.home, page: () => HomePage()),
    GetPage(name: RouteName.product, page: () => ProductPage()),
    GetPage(name: RouteName.product + '/:idProduct?', page: () => DetailPage()),
  ];
}