import 'package:belajargetx/controller/count_controller.dart';
import 'package:belajargetx/controller/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Controller
  final counterC = Get.put(CounterController());
  final themeC = Get.put(ThemeController());

  @override
  Widget build(BuildContext context) {
    return Obx(
        () => MaterialApp(
        theme: themeC.isDark.value ? ThemeData.dark() : ThemeData.light(),
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  final countC = Get.find<CounterController>();
  final themeC = Get.find<ThemeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Obx(
          () => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Angka ${countC.counter}",
                style: const TextStyle(fontSize: 30),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: ElevatedButton(
                  onPressed: () => themeC.switchTheme(),
                  child: const Text("Switch Theme"),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: ElevatedButton(
                        child: const Text(
                          '-',
                          style: TextStyle(fontSize: 20.0),
                        ),
                        onPressed: ()=> countC.funcDecrement(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: ElevatedButton(
                        child: const Text(
                          '+',
                          style: TextStyle(fontSize: 20.0),
                        ),
                        onPressed: ()=> countC.funcIncrement(),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}