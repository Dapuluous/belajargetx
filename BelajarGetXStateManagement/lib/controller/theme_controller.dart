import 'package:get/get.dart';

class ThemeController extends GetxController {
  var isDark = false.obs;
  void switchTheme() => isDark.value = !isDark.value;
}