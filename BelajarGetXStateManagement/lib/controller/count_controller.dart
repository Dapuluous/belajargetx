import 'package:get/get.dart';

class CounterController extends GetxController {
  var counter = 0.obs;
  void funcIncrement() => counter + 1;
  void funcDecrement() => counter - 1;
}