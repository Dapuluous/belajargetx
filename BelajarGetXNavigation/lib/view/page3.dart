import 'package:belajargetxnavigation/routes/app_page_name.dart';
import 'package:belajargetxnavigation/view/page4.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PageTiga extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Page Tiga"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () => Get.back(),
              child: Text("Back Page"),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: ElevatedButton(
                onPressed: () => Get.toNamed(AppPageName.page_tiga),
                child: Text("Next Page"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}