import 'package:belajargetxnavigation/routes/app_page_name.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PageDua extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Page Dua"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () => Get.back(),
              child: Text("Back Page"),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: ElevatedButton(
                onPressed: () => Get.toNamed(AppPageName.page_tiga),
                child: Text("Next Page"),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Text(Get.arguments),
            )
          ],
        ),
      ),
    );
  }
}