import 'package:belajargetxnavigation/routes/app_page_name.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PageSatu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Page Satu"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              // onPressed: () => Get.to(PageDua(), arguments: "Dari Page 1"),
              onPressed: () => Get.toNamed(AppPageName.page_dua),
              child: Text("Next Page"),
            ),
          ],
        ),
      ),
    );
  }
}