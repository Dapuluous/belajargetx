import 'package:belajargetxnavigation/routes/app_page_name.dart';
import 'package:belajargetxnavigation/view/page1.dart';
import 'package:belajargetxnavigation/view/page2.dart';
import 'package:belajargetxnavigation/view/page3.dart';
import 'package:belajargetxnavigation/view/page4.dart';
import 'package:get/get.dart';

class AppPageRoutes {
  static final pages = [
    GetPage(name: AppPageName.page_satu, page: () => PageSatu()),
    GetPage(name: AppPageName.page_dua, page: () => PageDua()),
    GetPage(name: AppPageName.page_tiga, page: () => PageTiga()),
    GetPage(name: AppPageName.page_empat, page: () => PageEmpat()),
  ];
}