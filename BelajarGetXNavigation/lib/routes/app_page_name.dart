abstract class AppPageName {
  static const page_satu = '/page_satu';
  static const page_dua = '/page_dua';
  static const page_tiga = '/page_tiga';
  static const page_empat = '/page_empat';
}