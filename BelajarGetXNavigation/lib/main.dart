import 'package:belajargetxnavigation/view/page1.dart';
import 'package:belajargetxnavigation/routes/app_page_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: PageSatu(),
      initialRoute: '/',
      // unknownRoute: GetPage(name: '/404', page: () => PageNotFound()),
      getPages: AppPageRoutes.pages,
    );
  }
}