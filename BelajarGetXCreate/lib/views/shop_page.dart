import 'package:belajargetxcreate/controller/shop_controller.dart';
import 'package:belajargetxcreate/widgets/shop_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShopPage extends StatelessWidget {
  final shopC = Get.put(ShopController(), tag: 'total');
  final quantityC = Get.create(() => ShopController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Center(
        child: ListView.builder(
          itemCount: 2,
          itemBuilder: (context, index) => ShopItem(),
        ),
      ),

      floatingActionButton: CircleAvatar(
        child: Obx(
            () => Text("${shopC.total}"),
        ),
      ),
    );
  }
}