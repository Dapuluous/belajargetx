import 'package:belajargetxcreate/views/shop_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Home Page"),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: OutlinedButton(
                  onPressed: () => Get.to(() => ShopPage()),
                  child: Text("Shop Page"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}