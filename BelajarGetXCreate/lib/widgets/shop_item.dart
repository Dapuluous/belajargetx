import 'package:belajargetxcreate/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShopItem extends StatelessWidget {
  final shopC = Get.find<ShopController>();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        IconButton(
            onPressed: () {
              shopC.quantity.value--;
              Get.find<ShopController>(tag: 'total').total.value--;
            },
            icon: Icon(Icons.remove),
        ),

        Obx(
              () => Text("${shopC.quantity}"),
        ),

        IconButton(
            onPressed: () {
              shopC.quantity.value++;
              Get.find<ShopController>(tag: 'total').total.value++;
            },
            icon: Icon(Icons.add)
        ),
      ],
    );
  }
}