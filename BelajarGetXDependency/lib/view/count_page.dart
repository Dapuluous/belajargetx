import 'package:belajargetxdependency/controller/my_controller.dart';
import 'package:belajargetxdependency/view/text_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CountPage extends StatelessWidget {
  final c = Get.put(MyController(), permanent: true, tag: 'tag1');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Count Page"),
        actions: [
          IconButton(
            onPressed: () => Get.to(() => TextPage()),
            icon: const Icon(Icons.keyboard_arrow_right),
          ),
        ],
      ),
      body: Center(
        child: Obx(() => Text("Angka ${c.count}")),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => c.add(),
      ),
    );
  }
}