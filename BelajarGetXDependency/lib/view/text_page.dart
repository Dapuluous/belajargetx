import 'package:belajargetxdependency/controller/my_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TextPage extends StatelessWidget {
  final c = Get.put(MyController(), permanent: false, tag: 'tag2');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Count Page"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Center(
          child: TextField(
            controller: c.textC,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
            ),
          ),
        ),
      ),
    );
  }
}