import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class MyController extends GetxController {
  var count = 0.obs;
  var textC = TextEditingController();

  void add() => count++;
}