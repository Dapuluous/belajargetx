import 'package:belajargetxlifecycle/controller/text_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TextPage extends StatelessWidget {
  final textForm = Get.put(TextController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Text Page"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: TextField(
          controller: textForm.formNama,
        ),
      ),
    );
  }
}