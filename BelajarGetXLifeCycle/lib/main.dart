import 'package:belajargetxlifecycle/controller/count_controller.dart';
import 'package:belajargetxlifecycle/view/text_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final countC = Get.put(CountController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home"),
        actions: [
          IconButton(onPressed: () => Get.to(() => TextPage()), icon: const Icon(Icons.refresh)),
        ],
      ),
      body: const Center(
        // child: CountWidget(),
        child: Text("Home Page"),
      ),

      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     setState(() {
      //       countC.add();
      //     });
      //   },
      // ),
    );
  }
}

class CountWidget extends StatelessWidget {
  final countC = Get.find<CountController>();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CountController>(
      initState: (_) => print("Init State"),
      didChangeDependencies: (state) => print("Did Change"),
      didUpdateWidget: (oldWidget, state) => print("Did Update"),
      dispose: (state) => print("Dispose"),
      builder: (c) => Text("Angka ${c.count}"),
    );
  }
}


