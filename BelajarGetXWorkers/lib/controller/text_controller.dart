import 'package:get/get.dart';

class TextController extends GetxController {
  var count = 0.obs;
  void change() => count++;
  void reset() => count.value = 0;

  @override
  void onInit() {
    print("On Init");

    // ever(count, (_) => print("Run"));
    // everAll([count], (_) => print("Run"));
    // once(count, (_) => print("Run"));

    // Buat Pencarian Otomatis
    // debounce(count, (_) => print("Run"), time: Duration(seconds: 3));

    // Buat Interval per x detik
    // interval(count, (_) => print("Run"), time: Duration(seconds: 3));

    super.onInit();
  }
}