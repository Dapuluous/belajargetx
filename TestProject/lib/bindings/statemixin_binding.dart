import 'package:get/get.dart';
import 'package:testproject/controllers/statemixin_controller.dart';

class StateMixinBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(StateMixinController());
  }
}