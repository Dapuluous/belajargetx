import 'package:get/get.dart';
import 'package:testproject/bindings/count_binding.dart';
import 'package:testproject/bindings/statemixin_binding.dart';
import 'package:testproject/routes/app_pages.dart';
import 'package:testproject/views/configuration_views/settings.dart';
import 'package:testproject/views/home_page.dart';
import 'package:testproject/views/starter_page/starter_page.dart';
import 'package:testproject/views/statemixin_views/statemixin_home.dart';

class AppRoutes {
  static final pages = [
    GetPage(
        name: AppPages.homePage,
        page: () => HomePage()
    ),
    GetPage(
        name: AppPages.starterPage,
        page: () => StarterPage(),
        binding: CountBinding()
    ),
    GetPage(
        name: AppPages.mixinHome,
        page: () => StatemixinHome(),
        binding: StateMixinBinding()
    ),
    GetPage(
        name: AppPages.settings,
        page: () => Settings()
    ),
  ];
}