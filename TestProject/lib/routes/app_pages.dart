abstract class AppPages {
  static const homePage = '/';
  static const starterPage = '/starterPage';
  static const mixinHome = '/mixinHome';
  static const settings = '/settings';
}