import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testproject/routes/app_routes.dart';
import 'package:testproject/utils/translations.dart';
import 'package:testproject/views/error_views/not_found.dart';
import 'package:testproject/views/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      translations: MyTranslation(),
      locale: Locale('id'),
      theme: ThemeData.light(),
      home: HomePage(),
      initialRoute: '/',
      unknownRoute: GetPage(name: '/404', page: () => PageNotFound()),
      getPages: AppRoutes.pages,
    );
  }
}