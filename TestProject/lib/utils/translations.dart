import 'package:get/get.dart';

class MyTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
    'en' : {
      'title': 'Flutter Demo Home Page',
      'body': 'You have pushed the button this many times:'
    },
    'id': {
      'title': 'Halaman Demo Flutter',
      'body': 'Kamu telah menekan tombol ini sebanyak:'
    }
  };
}