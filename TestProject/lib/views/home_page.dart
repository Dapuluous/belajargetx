import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testproject/routes/app_pages.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       title: Text("Home"),
       actions: [
         IconButton(
             onPressed: () => Get.toNamed(AppPages.settings),
             icon: Icon(Icons.settings),
         ),
       ],
     ),
     body: Center(
       child: Column(
         mainAxisAlignment: MainAxisAlignment.center,
         children: [
           ElevatedButton(
             onPressed: () => Get.toNamed(AppPages.starterPage),
             child: const Text("Starter Flutter App"),
           ),

           Padding(
             padding: EdgeInsets.only(top: 8),
             child: ElevatedButton(
               onPressed: () => Get.toNamed(AppPages.mixinHome),
               child: const Text("Statemixin Home"),
             ),
           ),
         ],
       ),
     ),
   );
  }
}