import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testproject/controllers/statemixin_controller.dart';

class StatemixinHome extends GetView<StateMixinController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      body: Center(
        child: controller.obx((state) => Text(
          "Nama: ${state!["first_name"]}"
        ),
          onLoading: Text("Loading..."),
          onEmpty: Text("Belum ada data."),
          onError: (error) => Text(error.toString())
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => controller.getData(),
        child: Text("+"),
      ),
    );
  }
}