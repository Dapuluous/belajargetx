import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Dark Mode"),
              
              ElevatedButton(
                onPressed: () {
                  Get.changeTheme(
                    Get.isDarkMode ? ThemeData.light() : ThemeData.dark(),
                  );
                },
                child: Text("Switch Theme"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}