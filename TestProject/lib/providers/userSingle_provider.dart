import 'package:get/get.dart';

class UserSingleProvider extends GetConnect {
  Future<Response> getUser() => get('https://reqres.in/api/users/2');
}